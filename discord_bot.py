import discord
import keyfile
import priceCheck
import asyncio
import priceCheckHistory
import items

client = discord.Client()

# Variable to keep track of last price alert sent
old_msg = " "


def init_bot(item_dic, testing):

    @client.event
    async def on_message(message):
        """
        Takes in the input of a user on discord.
        1. Checks to make sure it isn't the bot itself that commands it.
        2.  If the keyword is !snipe the bot sends a message in discord informing that it has started looking and
            starts a (loop?) for running the pricealert function?
        :param message: input form discord user.
        """
        # we do not want the bot to reply to itself
        if message.author == client.user:
            return

        if message.content.startswith('!snipe') and not testing:
            msg = 'sniper started {0.author.mention}'.format(message)
            await client.send_message(message.channel, msg)
            await client.purge_from(message.channel, limit=2, check=None)
            client.loop.create_task(pricealert(message))

        if message.content.startswith('?snipe') and testing:
            msg = 'sniper started {0.author.mention}'.format(message)
            await client.send_message(message.channel, msg)
            await client.purge_from(message.channel, limit=2, check=None)
            client.loop.create_task(pricealert(message))

        if message.content.startswith('?history') and testing:
            msg = await priceCheckHistory.check_price_history_alert_values()
            await client.send_message(message.channel, msg)

        if message.content.startswith('!history') and not testing:
            msg = await priceCheckHistory.check_price_history_alert_values()
            await client.send_message(message.channel, msg)



    async def pricealert(message):
        """
        Function that runs while the clients isn't closed after it has been called, and starts calling the
            pricecheck.alert with a 60 second timer between each call.
        :param message: Message data of where the message was sent(?)
        """
        time_to_history = 0
        while not client.is_closed:
            await priceCheck.alert(item_dic, client, message, old_msg)
            await asyncio.sleep(1)
            time_to_history += 1
            print (time_to_history)
            if time_to_history == 1440:
                priceCheckHistory.price_check_history(items.item_dic)
                time_to_history = 0
        msg = 'I Died :( {0.author.mention}'.format(message)  # If it exits the loop randomly.
        await client.send_message(message.channel, msg)

    @client.event
    async def on_ready():
        """
        Tells the user inside the terminal that the bot is now active and running.
        """
        print('Logged in as')
        print(client.user.name)
        print(client.user.id)
        print('------')

    if testing:
        client.run(keyfile.dev_bot_token)
    else:
        client.run(keyfile.bot_token)


async def message_alert(data, client, message):
    """
    Runs when an item is found that is cheaper than it should be and blasts the message underneath onto the
    discord channel that started the !snipe.
    :param data: Data of the item that should be blasted onto discord.
    :param client: client info(?) probably needed.
    :param message: Message info, needed to tell the program where to send the message.
    """
    await client.send_message(message.channel, "PRICE ALERT!!! "
                                               + str(data["Item"]["Name"])
                                               + " Mispriced at: "
                                               + priceCheck.price_format(data))