# Functions for reading an item file into an array of items and input for new items.

import requests
import keyfile


# Dictionary of items used in the program
item_dic = {

}

# Pegasus = 16564, Dalamud = 5738, earth shard = 5


def add_item(name, id):
    """
    Function for adding a new item into the dictionary, the history variable gets added when a history lookup is
    performed.
    :param name: Name of the item, gets provided by the item_name_get function
    :param id: ID of the item provided by the user
    """
    item_dic[id] = {}
    item_dic[id]["name"] = name
    item_dic[id]["ID"] = id
    item_dic[id]["hard_cheapest"] = 0


def remove_item(id):
    """
    Function to delete an item in the item dictionary.
    :param name: ID of the item
    """
    del item_dic[id]


def item_name_get(id):
    """
    Function for looking up the item name and providing this to the creation of new items (I don't trust myself
    to write them correctly)
    :param id: ID of the item added
    :return: Returns the name of the item after lookup has been performed
    """
    response = requests.get("https://xivapi.com/market/omega/items/" + id
                            + "?key=" + keyfile.api_key)
    data = response.json()
    return str(data["Item"]["Name"])


def item_history(id, price):
    """
    Function for setting the history record
    :param id: ID of the item to have it's "cheapest" history price set
    :param price: The "cheapest" history price
    """
    item_dic[id]["cheapest"] = price

def item_hard_cheapest(id):
    """
    
    :param id: 
    :return: 
    """
    hard_cheapest = input("Set item price for alert")
    item_dic[id]["hard_cheapest"] = hard_cheapest


def write_items_to_file():
    """
    Writes the item_dic variable onto a json file
    """
    import json
    with open("item_dic.json", 'w') as f:
        json.dump(item_dic, f)


def read_items_from_file():
    """
    Opens and reads the item_dic.json file and loads it into a loaded_items variable (for some reason I couldn't set the
    item_dic variable and had to use a return statement)
    :return: returns the loaded_items variable so that the item_dic can be set
    """
    import json
    with open("item_dic.json") as f:
        loaded_items = json.load(f)
        return loaded_items
