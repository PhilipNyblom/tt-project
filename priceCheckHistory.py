import requests
import keyfile
import items


def price_check_history(item_dic):
    """
    Goes through the price history of every item in the item_dic and tries to find out what the most reasonable
    cheapest price it has been sold at is, so per now it checks if the price per unit is less than 50% cheaper than the
    last one sold, if it is the item is deemed mispriced and overlooked for the cheapest history variable
    second misprice check is if the next item in the transaction history is twice as expensive as the last transaction
    if this is true then the last transaction is deemed mispriced and the second to last is the new "cheapest" after
    this it's all just checks to see if the item is cheaper or not, if it is it becomes the new cheapest, if not
    the old cheapest stays.
    When this is done the item cheapest value is set in the items.py file.
    :param item_dic: the dictionary of the items to be used in the program
    """
    for item in item_dic:
        if not item_dic[item]["hard_cheapest"]:
            response = requests.get("https://xivapi.com/market/omega/items/" + item_dic[item]["ID"]
                                    + "/history?key=" + keyfile.api_key)
            data = response.json()
            transactions = data["History"]
            cheapest_history = transactions[0]["PricePerUnit"]
            total = 0.0
            num_transactions = 0.0

            for transaction in transactions:
                if transaction["PricePerUnit"] < (cheapest_history*0.7):
                    pass
                elif transaction["PricePerUnit"] > (cheapest_history*2):
                    cheapest_history = transaction["PricePerUnit"]
                elif transaction["PricePerUnit"] < cheapest_history:
                    cheapest_history = transaction["PricePerUnit"]
                total += transaction["PricePerUnit"]
                num_transactions += 1
            total = total / num_transactions
            if total > cheapest_history:
                cheapest_history = total

            items.item_history(item_dic[item]["ID"], cheapest_history)


async def check_price_history_alert_values():
    hist = ""
    for item in items.item_dic:
        hist = (hist + str(items.item_dic[item]["name"])
                + " cheapest at: "
                + format(int(items.item_dic[item]["cheapest"]),",d")
                + "\n \t")
        if int(items.item_dic[item]["hard_cheapest"]) > 0:
            hist = (hist + " Hard set alert value: "
                    + format(int(items.item_dic[item]["hard_cheapest"]), ",d"))
        else:
            hist = (hist + " Alert value: "
                    + format(int(items.item_dic[item]["cheapest"] * 0.8),",d"))
        hist = hist + "\n"

    return hist


def check_price_cheapest(item, item_dic):
    """
    Checks what the cheapest the item given has had.
    :param item: ID for the item to be checked.
    :param item_dic: The dictionary for where the item lies.
    :return: The "cheapest" value the item has had.
    """
    return item_dic[item]["cheapest"]
