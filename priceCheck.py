import requests
import keyfile
import priceCheckHistory
import asyncio
import aiohttp
import discord_bot
import logging
import datetime
import json


async def fetch(session, url):
    async with session.get(url) as response:
        return await response.json()


async def price_check(item_dic):
    """
    Takes the whole item dictionary and lists out the name and price per unit in an easily readable format
        Currently broken when called from main
    :param item_dic: The item-dictionary from where the items to list lie
    """
    for item in item_dic:
        data = await individual_item(item, item_dic)
        print("Cheapest " + data["Item"]["Name"] + " cost = " + str(data["Prices"][0]["PricePerUnit"]))


async def individual_item(item):
    """
    Lookup for getting the price of a given item
    :param item: Item ID of the item to be checked
    :param item_dic: The item dictionary from where the item id is defined
    :return: Returns the response from the server after the lookup and some (json parsing?) in a dictionary format
    """
    async with aiohttp.ClientSession() as session:
        response = await fetch(session, "https://xivapi.com/market/omega/items/" + item
                           + "?key=" + keyfile.api_key)

        #    aiohttp.get("https://xivapi.com/market/omega/items/" + item_dic[item]["ID"]
        #                  + "?key=" + keyfile.api_key) as response:
        data = response #json.loads(response)
    return data


async def alert(item_dic, client, message, oldmsg):
    """
    The function goes through all elements in the dictionary.
        1. They get the individual item request dictionary with all the items currently listed.
        2. The first item is made into a message.
        3. Printing some item data, for visual feedback in console that the checking is running.
        4.Checks if the top price of the item which will always be the cheapest is cheaper than the price history by
            20 % and it checks if its message is the same as the old message (to avoid spam).
        5. If this if statement goes true then the price alert goes out through the discord channel and the new message
            becomes the old_msg.
    :param item_dic: Item dictionary for what items should be price checked.
    :param client: Client variable used by the Discord.py, unsure what it does but seems like its needed.
    :param message: This is the message variable the discord.py use to determine what channel its messages should go to.
    :param oldmsg: To keep the bot from writing out the same message multiple times after each other.
    """
    for item in item_dic:
        data = await individual_item(item)

        try:
            if "Error" in data and data["Error"]:
                print("Server error!")
                if "Message" in data:
                    print("Message: " + data["Message"])
            else:
                msg = str("PRICE ALERT!!! " + str(data["Item"]["Name"]) + " Mispriced at: "
                          + price_format(data))

                if bool(item_dic[item]["hard_cheapest"]):
                    if data["Prices"][0]["PricePerUnit"] < int(item_dic[item]["hard_cheapest"]) and oldmsg != msg:
                        await discord_bot.message_alert(data, client, message)
                        discord_bot.old_msg = str("PRICE ALERT!!! " + str(data["Item"]["Name"]) + " Mispriced at: "
                                              + price_format(data))
                else:
                    if (data["Prices"][0]["PricePerUnit"] < priceCheckHistory.check_price_cheapest(item, item_dic) * 0.8
                            and oldmsg != msg):
                        await discord_bot.message_alert(data, client, message)
                        discord_bot.old_msg = str("PRICE ALERT!!! " + str(data["Item"]["Name"]) + " Mispriced at: "
                                              + price_format(data))

                print(data["Item"]["Name"] + "     Time: " + str(datetime.datetime.now().strftime('%H:%M:%S')))
                price_format(data)

        except (KeyError, IndexError) as e:
            print(str(data) + '\n')
            logging.error(str(e))


def price_format(data):
    price_len = len(str(data["Prices"][0]["PricePerUnit"]))
    spaced_price = " "
    while price_len >= 1:
        if price_len >= 3:
            spaced_price = str((data["Prices"][0]["PricePerUnit"]))[(price_len - 3):price_len] + " " + spaced_price
        elif price_len >= 2:
            spaced_price = str((data["Prices"][0]["PricePerUnit"]))[(price_len - 2):price_len] + " " + spaced_price
        elif price_len >= 1:
            spaced_price = str((data["Prices"][0]["PricePerUnit"]))[(price_len - 1):price_len] + " " + spaced_price
        price_len = price_len - 3
    return spaced_price
