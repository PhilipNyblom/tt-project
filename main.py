import items
import priceCheck
import priceCheckHistory
import discord_bot
import logging
import asyncio


def main():
    option = 0
    testing = True
    items.item_dic = items.read_items_from_file()
    priceCheckHistory.price_check_history(items.item_dic)
    while option != 'q':
        if testing:
            print("You are in dev mode\n")
        else:
            print("You are in release mode\n")
        option = input("Options: \n"
                       "q to quit \n"
                       "h to check price history\n"
                       "a add new item to list\n"
                       "r to remove an item on the list\n"
                       "t to go between dev and release mode\n"
                       "s to hard set a cheapest value \n"
                       "t to change between release and dev bot \n"
                       "b to initiate bot\n")
        option = option.lower()
        if option == 'h':
            print(priceCheckHistory.check_price_history_alert_values())
        elif option == 'a':
            idnum = input("what is the ID number of the item you wish to add? \n")
            items.add_item(items.item_name_get(idnum), idnum)
            priceCheckHistory.price_check_history(items.item_dic)
            items.write_items_to_file()
        elif option == 'r':
            idnum = input("what is the ID number of the item you wish to remove? \n")
            items.remove_item(idnum)
        elif option == 'b':
            try:
                discord_bot.init_bot(items.item_dic, testing)
            except Exception as e:              # Does this catch errors too?
                items.write_items_to_file()
                logging.error(str(e))
                raise SystemExit
        elif option == 's':
            idnum = input("what is the ID number of the item you wish set a value for? \n")
            items.item_hard_cheapest(idnum)
        elif option == 't':
            testing = not testing
        elif option == 'q':
            items.write_items_to_file()
            break


if __name__ == "__main__":
    main()

